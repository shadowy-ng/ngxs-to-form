import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ngxs-to-form-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  title = 'app-ngxs-to-form';
}
