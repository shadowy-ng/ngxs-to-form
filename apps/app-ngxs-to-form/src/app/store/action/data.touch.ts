import { ngxsAction } from '@shadowy/ngxs-to-form';

@ngxsAction('data:touch')
export class DataTouch {
  public static type = '[Data] Touch';

  constructor(public payload: string) {}
}
