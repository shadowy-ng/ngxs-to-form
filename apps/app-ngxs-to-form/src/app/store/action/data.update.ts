import { INgxsDataChange, ngxsAction } from '@shadowy/ngxs-to-form';

@ngxsAction('data')
export class DataUpdate {
  public static readonly type = '[Data] Update';

  constructor(public payload: INgxsDataChange) {}
}
