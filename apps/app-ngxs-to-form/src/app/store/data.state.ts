import { Action, Selector, State, StateContext } from '@ngxs/store';
import { applyChanges, ngxsData } from '@shadowy/ngxs-to-form';
import { DataTouch, DataUpdate } from './action';
import { defaultDataState, IData, IDataStateModel } from './data.state.model';

@State<IDataStateModel>(defaultDataState)
export class DataState {
  @ngxsData('data') @Selector() public static getModel(state: IDataStateModel): Partial<IData> {
    return state.model;
  }

  @ngxsData('data:touch') @Selector() public static get(state: IDataStateModel): { [key: string]: boolean } {
    return state.touched;
  }

  @ngxsData('data:validation') @Selector() public static getValidation(): { [key: string]: string } {
    return {};
  }

  @Action(DataUpdate) update({ getState, setState }: StateContext<IDataStateModel>, { payload }: DataUpdate) {
    return applyChanges(getState, setState, payload, 'model');
  }

  @Action(DataTouch) touch({ getState, patchState }: StateContext<IDataStateModel>, { payload }: DataTouch) {
    const { touched } = getState();
    touched[payload] = true;
    patchState({ touched: { ...touched } });
  }
}
