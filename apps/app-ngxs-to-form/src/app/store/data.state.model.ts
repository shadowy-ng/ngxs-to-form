export interface IData {
  field1?: string;
  field2?: string;
  field3?: number;
}

export interface IDataStateModel {
  model: IData;
  validation: { [key: string]: string };
  touched: { [key: string]: boolean };
}

export const defaultDataState = {
  name: 'data',
  defaults: {
    model: {},
    validation: {},
    touched: {},
  },
};
