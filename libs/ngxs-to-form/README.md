# Ngxs-To-Form
[![pipeline status](https://gitlab.com/shadowy-ng/ngxs-to-form/badges/master/pipeline.svg)](https://gitlab.com/shadowy-ng/ngxs-to-form/commits/master)
[![coverage report](https://gitlab.com/shadowy-ng/ngxs-to-form/badges/master/coverage.svg)](https://shadowy-ng.gitlab.io/ngxs-to-form/)

## Install

Using npm:
``` bash
npm install @shadowy/ngxs-to-form
```
or using yarn:
``` bash
yarn add @shadowy/ngxs-to-form
```
